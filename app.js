// 
// auth: Fadi Mabsaleh <fadimoubassaleh@gmail.com>
// 
// description: the main file of the server
// use:     copy .example.env -> change name to .env -> change all the variables
//          
// 
// 

const express = require('express');
const app = express();
const dotenv = require('dotenv'); // env

dotenv.config(); // run env variables

// Middleware
app.use(express.json());
//

app.post('/', function(res, res) {
    res.status(200).json({
        success: true,
        message: [
            'server is running smoothly'
        ]
    })
})

// run server (node - express)
// server variables
const port = process.env.SERVER_PORT
    // 
app.listen(port, () => console.log('Express run on port ' + port))
    //